import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter, Route } from 'react-router-dom';

//Import Frontend Component
import App from './App';
//Import Login Component
import Login from './Login';
//Import ProfileDetail Component
import ProfileDetail from "./ProfileDetail";
//Import CSS
import './index.css';

//Main Component
class Entry extends React.Component {
    render() {
        return (
            //Route to component
            <BrowserRouter>
                <div>
                    <Route exact path="/" component={App} />
                    <Route path="/login" component={Login} />
                    <Route path="/admin" component={Login} />
                    <Route path="/users/:id" component={ProfileDetail} />
                </div>
            </BrowserRouter>
        )
    }
}

//Render Main Component
ReactDOM.render(<Entry />, document.getElementById('root'));

