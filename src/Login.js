import React, { Component } from 'react';
//Import authentication from Firebase
import { auth } from './firebase';
//Import Admin Component
import Admin from './Admin';


class Login extends Component {
    constructor(props) {
        super(props);
        this.state = {
            //Store login/register email and password
            //Also if user is logged in or not
            email: "",
            emailIsValid: null,
            password: "",
            passwordIsvalid: null,
            loginEmail: "",
            loginEmailIsValid: null,
            loginPassword: "",
            loginPasswordIsValid: null,
            loggedIn: false
        }
        //Bind this instance
        this.handleEmailInput = this.handleEmailInput.bind(this);
        this.handlePasswordInput = this.handlePasswordInput.bind(this);
        this.handleLoginEmailInput = this.handleLoginEmailInput.bind(this);
        this.handleLoginPasswordInput = this.handleLoginPasswordInput.bind(this);
        this.handleSubmitRegister = this.handleSubmitRegister.bind(this);
        this.handleSubmitLogin = this.handleSubmitLogin.bind(this);
        this.verifyEmail = this.verifyEmail.bind(this);

    }
    //On component mount, check if user is logged in
    componentDidMount() {

        auth.onAuthStateChanged((user) => {
            if (user) {
                this.setState({
                    loggedIn: true
                })
            } else {
                this.setState({
                    loggedIn: false
                })
            }
        })

    }
    //Handle Input for login and register email and password fields
    handleEmailInput(e) {
        let email = e.target.value;
        this.setState({
            email
        })
    }

    handlePasswordInput(e) {
        let password = e.target.value;
        this.setState({
            password
        })
    }

    handleLoginEmailInput(e) {
        let email = e.target.value;
        this.setState({
            loginEmail: email
        })
    }

    handleLoginPasswordInput(e) {
        let password = e.target.value;
        this.setState({
            loginPassword: password
        })
    }

    //If new user registration, send verification mail
    verifyEmail() {
        let user = auth.currentUser;
        user.sendEmailVerification().then(() => {
        }).catch((error) => {
            console.log(error);
        })
    }

    //On registration, create a new user in firbase and call the verify emaill function
    handleSubmitRegister(e) {
        e.preventDefault();
        let email = this.state.email;
        let password = this.state.password;

        auth.createUserWithEmailAndPassword(email, password)
            .catch((error) => {
                alert(error.message);
                if (error) {
                    return false
                } else {
                    return true
                }
            })
            .then((e) => {
                if (e) {
                    return this.verifyEmail();
                }
            })

    }
    //On Login, Sign In in firebase, 
    handleSubmitLogin(e) {
        e.preventDefault();
        let email = this.state.email;
        let password = this.state.password;
        auth.signInWithEmailAndPassword(email, password)
            .catch((error) => {
                alert(error.message);
            })
    }

    render() {
        return (

            < div >
                {/* If loggedin show admin component, else show login/registration form */}
                {!this.state.loggedIn ? (
                    <div>
                        <h1>Register</h1>
                        <form onSubmit={this.handleSubmitRegister}>
                            <input
                                type="text"
                                placeholder="Email"
                                value={this.state.email}
                                onChange={this.handleEmailInput}
                                className={this.state.emailIsValid} />

                            <input
                                type="password"
                                placeholder="Password"
                                value={this.state.password}
                                onChange={this.handlePasswordInput}
                                className={this.state.passwordIsValid} />


                            <input type="submit" value="Submit" />
                        </form>

                        <div>
                            <h1>Login</h1>
                            <form onSubmit={this.handleSubmitLogin}>
                                <input
                                    type="text"
                                    placeholder="Email"
                                    value={this.state.loginEmail}
                                    onChange={this.handleLoginEmailInput}
                                    className={this.state.loginEmailIsValid} />

                                <input
                                    type="password"
                                    placeholder="Password"
                                    value={this.state.loginPassword}
                                    onChange={this.handleLoginPasswordInput}
                                    className={this.state.loginPasswordIsValid} />


                                <input type="submit" value="Submit" />
                            </form>
                        </div>
                    </div>
                ) : (
                        <Admin />
                    )
                }
            </div>
        );
    }
}

//Export Component
export default Login;