import React, { Component } from 'react';
//Import database and auth from firebase
import { database, auth } from './firebase';
import { Link, Redirect } from 'react-router-dom';
//Import Login component
import Login from './Login';


class ProfileDetail extends Component {
    constructor(props) {
        super(props);
        this.state = {
            //state to keep user details and state to check if user is loggged in
            isUserLoggedIn: false,
            userEmail: "",
            userName: "",
            userOption: "",
            userWebAddress: "",
            userCoverLetter: ""
        };
        this.handleSignOut = this.handleSignOut.bind(this);

    }

    //Get data of the selected profile and set it to state
    //Check if user has verified mail
    componentDidMount() {
        let email = this.props.match.params.id;
        database.ref().child('profiles').on('value', (snapshot) => {
            snapshot.forEach((child) => {
                let findEmail = child.val();
                if (email === findEmail['email']) {
                    var userEmail = findEmail['email'];
                    var userName = findEmail['name'];
                    var userOption = findEmail['choice'];
                    var userWebAddress = findEmail['webAddress'];
                    var userCoverLetter = findEmail['coverLetter'];
                    this.setState({
                        userEmail,
                        userName,
                        userOption,
                        userWebAddress,
                        userCoverLetter
                    })
                }
            })
        })

        auth.onAuthStateChanged((user) => {
            if (user) {
                var emailVerified = user.emailVerified;
                if (emailVerified) {
                    this.setState({
                        isUserLoggedIn: true
                    })
                } else {
                    this.setState({
                        isUserLoggedIn: false
                    })
                }
            } else {
                this.setState({
                    isUserLoggedIn: false
                })
            }
        })
    }
    //Sign out from firebase
    handleSignOut() {
        auth.signOut().then(() => {
            console.log('signed out');
        }).catch((error) => {
            console.log(error)
        })
    }

    render() {
        return (
            <div>
                {/* If user's email is verified show data otherwise send them to login component */}
                {this.state.isUserLoggedIn ?
                    <div>
                        <h1>{this.state.userName}</h1>
                        <h2>{this.state.userEmail}</h2>
                        <h3> Do you like coding: {this.state.userOption}</h3>
                        <a href={this.state.userWebAddress}>{this.state.userWebAddress}</a>
                        <h2>Cover Letter</h2>
                        <p>{this.state.userCoverLetter}</p>

                        <button onClick={this.handleSignOut}>Sign Out</button>
                        <Link to="/admin"><button>Go Back</button></Link>
                    </div>
                    :
                    <Login />}
            </div>
        );
    }
}
//Export
export default ProfileDetail;