import React, { Component } from 'react';
//Import Database Instance from Firebase  
import { database } from './firebase';
//Import Link from router
import { Link } from 'react-router-dom';
//Import google recaptcha
var Recaptcha = require('react-recaptcha');

//Frontend Component
class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      //Store form values and validity of values in state
      name: "",
      nameIsValid: null,
      email: "",
      emailIsValid: null,
      url: "",
      urlIsValid: null,
      letter: "",
      letterIsValid: null,
      option: "C",
      optionIsValid: null,
      recaptcha: false,
      recaptchaIsValid: null
    };
    //Bind this instance to the functions
    this.handleNameInput = this.handleNameInput.bind(this);
    this.handleEmailInput = this.handleEmailInput.bind(this);
    this.handleUrlInput = this.handleUrlInput.bind(this);
    this.handleLetterInput = this.handleLetterInput.bind(this);
    this.handleOptionInput = this.handleOptionInput.bind(this);
    this.verifyCallback = this.verifyCallback.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.submitToDatabase = this.submitToDatabase.bind(this);
    this.callback = this.callback.bind(this);
  }

  componentDidMount() {

  }



  //Regex test name, email, url, letter, question and recaptcha on input
  //If regex fails, turn form field RED
  //If correct, turn Green
  handleNameInput(e) {
    let name = e.target.value;
    const regexName = new RegExp("^[a-zA-Z ]+$");
    let checkName = regexName.test(name) ?
      this.setState({
        nameIsValid: true
      }) :
      this.setState({
        nameIsValid: false
      });
    this.setState({
      name
    })
  }

  handleEmailInput(e) {
    let email = e.target.value;
    var regexEmail = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    let checkEmail = regexEmail.test(email) ?
      this.setState({
        emailIsValid: true
      }) :
      this.setState({
        emailIsValid: false
      });
    this.setState({
      email
    })
  }
  handleUrlInput(e) {
    let url = e.target.value;
    var regexUrl = /[-a-zA-Z0-9@:%_\+.~#?&//=]{2,256}\.[a-z]{2,4}\b(\/[-a-zA-Z0-9@:%_\+.~#?&//=]*)?/gi;
    let checkUrl = regexUrl.test(url) ?
      this.setState({
        urlIsValid: true
      }) :
      this.setState({
        urlIsValid: false
      });
    this.setState({
      url
    })
  }

  handleLetterInput(e) {
    let letter = e.target.value;
    var regexletter = /^.{1,2000}$/;
    let checkLetter = regexletter.test(letter) ?
      this.setState({
        letterIsValid: true
      }) :
      this.setState({
        letterIsValid: false
      });
    this.setState({
      letter
    })
  }

  handleOptionInput(e) {
    let option = e.target.value;
    this.setState({
      option
    })
    if (option === "C") {
      this.setState({
        optionIsValid: false
      })
    } else {
      this.setState({
        optionIsValid: true
      })
    }
  }

  //Recaptcha verify callback
  verifyCallback(e) {
    if (e) {
      this.setState({
        recaptcha: true,
        recaptchaIsValid: true
      })
    }
  }
  //Recaptcha onload callback
  callback(e) {

  }

  //On Submit, regex test all fields
  handleSubmit(e) {
    e.preventDefault();
    if (!this.state.nameIsValid) {
      this.setState({
        nameIsValid: false
      })
    }
    if (!this.state.emailIsValid) {
      this.setState({
        emailIsValid: false
      })
    }
    if (!this.state.urlIsValid) {
      this.setState({
        urlIsValid: false
      })
    }
    if (!this.state.letterIsValid) {
      this.setState({
        letterIsValid: false
      })
    }
    if (this.state.option == "C") {
      this.setState({
        optionIsValid: false
      })
    }
    if (!this.state.recaptcha) {
      const alert = <h1>Please prove you are Human</h1>;
      this.setState({
        recaptchaIsValid: alert
      })
    }
    //If all field validate, then check if email exists, if not then Submit
    var emailIsNew = true;
    if (this.state.nameIsValid && this.state.emailIsValid && this.state.urlIsValid && this.state.letterIsValid && this.state.optionIsValid && this.state.recaptcha) {

      //Check if email exists
      database.ref().child('emails').on("value", (snapshot) => {
        snapshot.forEach((child) => {
          var email = this.state.email;
          var checkEmail = child.val();
          if (email === checkEmail) {
            emailIsNew = false
          }
        })
      })
      //Call submitToDatabase if email is new
      setTimeout(() => {
        if (emailIsNew) {
          this.submitToDatabase();
        } else {
          alert("Email is already submitted");
        }
      }, 700)

    }
  }

  //Submit data to data and clear input values  
  submitToDatabase() {
    var name = this.state.name;
    var email = this.state.email;
    var url = this.state.url;
    var letter = this.state.letter;
    var option = this.state.option;
    var data = {
      name: name,
      email: email,
      webAddress: url,
      coverLetter: letter,
      choice: option
    }
    //Push data
    database.ref().child('profiles').push(data);
    database.ref().child('emails').push(data.email);
    //Clear Input Fields
    this.setState({
      name: "",
      nameIsValid: null,
      email: "",
      emailIsValid: null,
      url: "",
      urlIsValid: null,
      letter: "",
      letterIsValid: null,
      option: "C",
      optionIsValid: null
    })
    alert("Data Submitted, Thanks!!");
  }


  render() {
    return (
      <div>
        <h1>Form</h1>
        <form onSubmit={this.handleSubmit}>
          {/* Name */}
          <input
            type="text"
            placeholder="Full Name"
            value={this.state.name}
            onChange={this.handleNameInput}
            className={this.state.nameIsValid} />
          {/* Email */}
          <input
            type="text"
            placeholder="Email Address"
            value={this.state.email}
            onChange={this.handleEmailInput}
            className={this.state.emailIsValid} />
          {/* WebAddress */}
          <input
            type="text"
            placeholder="Web Address"
            value={this.state.url}
            onChange={this.handleUrlInput}
            className={this.state.urlIsValid} />
          {/* CoverLetter */}
          <input
            type="text"
            placeholder="Cover Letter"
            value={this.state.letter}
            onChange={this.handleLetterInput}
            className={this.state.letterIsValid} />
          {/* Select Option */}
          <select value={this.state.option} onChange={this.handleOptionInput} className={this.state.optionIsValid} >
            <option value="A">Yes</option>
            <option value="B">No</option>
            <option value="C">You like to code? I do!</option>
          </select>
          {/* Recaptcha */}
          <Recaptcha
            sitekey='6LddDCsUAAAAACnoanXBAcixn5bVLY9zg_RZ8Tuj'
            render="explicit"
            verifyCallback={this.verifyCallback}
            onloadCallback={this.callback}
            className={this.state.recaptchaIsValid}
          />
          {/* If recaptch is not valid, show error */}
          {this.state.recaptchaIsValid}

          <input type="submit" value="Submit" />
        </form>

        {/* Button to Admin */}
        <Link to="/login"> <button>Go To Admin</button></Link>
      </div>
    );
  }
}

//Export component
export default App;
