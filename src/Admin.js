import React, { Component } from 'react';
//Import firebase auth and database
import { auth, database } from './firebase'
import { Route, Link } from 'react-router-dom';


//Component to show lists
function ListItem(props) {
    return (
        <li><Link to={`/users/${props.email}`}>{props.name}</Link> {props.email} {props.webAddress}</li >
    );
}
class Admin extends Component {
    constructor(props) {
        super(props);
        this.state = {
            //store data searchfield and if email is verified
            emailVerified: null,
            data: [],
            search: ""
        }
        //Bind this
        this.handleSearchInput = this.handleSearchInput.bind(this);
        this.handleSignOut = this.handleSignOut.bind(this);

    }
    //Check if user email is verified, loop through database and push to state.data
    componentDidMount() {
        auth.onAuthStateChanged((user) => {
            if (user) {
                let emailVerified = user.emailVerified;
                console.log(emailVerified);
                this.setState({
                    emailVerified
                })
            }
        })
        var data = [];

        database.ref().child('profiles').on('value', (snapshot) => {
            snapshot.forEach((child) => {
                console.log(child.val());
                data.push(child.val());
                this.setState({
                    data
                })
            })
        })

    }
    //Handle search input field
    handleSearchInput(e) {
        var search = e.target.value;
        this.setState({
            search
        })
    }
    //Handle sign out
    handleSignOut() {
        auth.signOut().then(() => {
            console.log('signed out');
        }).catch((error) => {
            console.log(error)
        })
    }

    render() {
        return (
            <div >
                {/* If user email verified show content otherwise show message to verify email */}
                {this.state.emailVerified ?
                    <div>
                        <form onSubmit={this.handleSubmit}>
                            <input
                                type="text"
                                placeholder="Search Full Name"
                                value={this.state.search}
                                onChange={this.handleSearchInput}
                            />
                        </form>
                        <ul>
                            {this.state.data.map((child) =>
                                child.name.toLowerCase().indexOf(this.state.search.toLocaleLowerCase()) >= 0 ?

                                    <ListItem key={child.email} name={child.name} key={child.key} email={child.email} webAddress={child.webAddress} />
                                    :
                                    null

                            )}
                        </ul>
                        <button onClick={this.handleSignOut}>Sign Out</button>
                    </div>
                    :
                    <div>
                        <h1>Please verify your email by clicking on the link sent to your email ID</h1>
                        <button onClick={this.handleSignOut}>Sign Out</button>
                    </div>}
            </div >
        );
    }
}


//Export Component

export default Admin;