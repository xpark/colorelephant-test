import * as firebase from "firebase";

//Firebase API Keys
var config = {
    apiKey: "AIzaSyAiMsEdVC52UwppDM2_a3v2nyntBnZ5Xvk",
    authDomain: "colorelephant-6184a.firebaseapp.com",
    databaseURL: "https://colorelephant-6184a.firebaseio.com",
    projectId: "colorelephant-6184a",
    storageBucket: "",
    messagingSenderId: "409286040183"
};
//Initialize App
firebase.initializeApp(config);

//Export Firebase and authentication and database Instances
export default firebase;

export const auth = firebase.auth();

export const database = firebase.database();